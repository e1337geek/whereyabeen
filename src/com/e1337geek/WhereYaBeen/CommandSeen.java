package com.e1337geek.WhereYaBeen;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CommandSeen implements CommandExecutor {

    //Declare Variables
    private ResultSet result;
    private ResultSet eventLookupResult;
    private String query;
    private String eventLookupQuery;
    private String lastseen;
    private String world;
    private String server;
    private int eventid = 0;
    private String eventmessage = new String();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        String playerUUID;

        if(args.length<1) {
            sender.sendMessage(ChatColor.DARK_AQUA + "WhereYaBeen plugin created by e1337geek. Use /seen <playername> for a player's last seen data.");
        }
        else {
            playerUUID = UUIDApi.getUUID(args[0]);
            //Build Query
            query = "SELECT * FROM wyb_eventlog WHERE uuid = '" + playerUUID + "' ORDER BY timestamp DESC LIMIT 1;";


            try {
                if (Main.connection.isValid(10)) {
                    //Run Query
                    BukkitRunnable r = new BukkitRunnable() {
                        @Override
                        public void run() {
                            try {
                                Statement statement = Main.connection.createStatement();
                                result = statement.executeQuery(query);
                                if (result.next()) {
                                    lastseen = result.getString("timestamp");
                                    world = result.getString("world");
                                    server = result.getString("server");
                                    eventid = result.getInt("eventtype");
                                }
                                eventLookupQuery = "SELECT * FROM wyb_eventtypes WHERE id = " + eventid + ";";
                                eventLookupResult = statement.executeQuery(eventLookupQuery);
                                if (eventLookupResult.next()) {
                                    eventmessage = eventLookupResult.getString("message");
                                }

                                if (lastseen == null) {
                                    sender.sendMessage(ChatColor.BLUE + args[0] + " has not been seen on this server.");
                                } else if (eventmessage.compareToIgnoreCase("joined") == 0) {
                                    sender.sendMessage(ChatColor.YELLOW + "[" + lastseen + "] " + ChatColor.BLUE + args[0] + " " + ChatColor.GREEN + eventmessage + ChatColor.BLUE + " " + world + " on " + server);
                                } else {
                                    sender.sendMessage(ChatColor.YELLOW + "[" + lastseen + "] " + ChatColor.BLUE + args[0] + " " + ChatColor.RED + eventmessage + ChatColor.BLUE + " " + world + " on " + server);
                                }
                                resetVariables();
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    r.runTaskAsynchronously(Main.wyb);
                }
                else {
                    Main.wyb.connectSQL();
                    //Run Query
                    BukkitRunnable r = new BukkitRunnable() {
                        @Override
                        public void run() {
                            try {
                                Statement statement = Main.connection.createStatement();
                                result = statement.executeQuery(query);
                                if (result.next()) {
                                    lastseen = result.getString("timestamp");
                                    world = result.getString("world");
                                    server = result.getString("server");
                                    eventid = result.getInt("eventtype");
                                }
                                eventLookupQuery = "SELECT * FROM wyb_eventtypes WHERE id = " + eventid + ";";
                                eventLookupResult = statement.executeQuery(eventLookupQuery);
                                if (eventLookupResult.next()) {
                                    eventmessage = eventLookupResult.getString("message");
                                }

                                if (lastseen == null) {
                                    sender.sendMessage(ChatColor.BLUE + args[0] + " has not been seen on this server.");
                                } else if (eventmessage.compareToIgnoreCase("joined") == 0) {
                                    sender.sendMessage(ChatColor.YELLOW + "[" + lastseen + "] " + ChatColor.BLUE + args[0] + " " + ChatColor.GREEN + eventmessage + ChatColor.BLUE + " " + world + " on " + server);
                                } else {
                                    sender.sendMessage(ChatColor.YELLOW + "[" + lastseen + "] " + ChatColor.BLUE + args[0] + " " + ChatColor.RED + eventmessage + ChatColor.BLUE + " " + world + " on " + server);
                                }
                                resetVariables();
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    r.runTaskAsynchronously(Main.wyb);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    private void resetVariables() {
        result = null;
        eventLookupResult = null;
        query = null;
        eventLookupQuery = null;
        lastseen = null;
        world = null;
        server = null;
        eventid = 0;
        eventmessage = new String();
    }
}
