package com.e1337geek.WhereYaBeen;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.bukkit.Bukkit.getServerName;

public class Main extends JavaPlugin {

    //Global variable declarations
    public static Main wyb;
    public static Connection connection;
    public static int connectionAttempts;
    private static String currentServer;
    private static String DB_Server;
    private static String DB_Name;
    private static String DB_User;
    private static String DB_Password;
    private static String DB_URL;
    FileConfiguration config = getConfig();

    public static String getCurrentServer() {
        return currentServer;
    }

    public static String getDB_Server() {
        return DB_Server;
    }

    public static String getDB_Name() {
        return DB_Name;
    }

    public static String getDB_User() {
        return DB_User;
    }

    public static String getDB_Password() {
        return DB_Password;
    }

    @Override
    public void onEnable() {
        //Fired when the server enables the plugin

        //Create instance of Main class for use in other classes
        wyb = this;

        //Set server name
        currentServer = getServerName();

        //Set up config.yml file
        config.addDefault("DB_Server", "localhost:3306");
        config.addDefault("DB_Name", "[YOUR DATABASE NAME]");
        config.addDefault("DB_User", "[YOUR DATABASE USER]");
        config.addDefault("DB_Password", "[YOUR DATABASE PASSWORD]");
        config.options().copyDefaults(true);
        saveConfig();

        //Set mySQL variables
        DB_Server = config.getString("DB_Server");
        DB_Name = config.getString("DB_Name");
        DB_User = config.getString("DB_User");
        DB_Password = config.getString("DB_Password");
        DB_URL = "jdbc:mysql://" + DB_Server + "/" + DB_Name;

        //Test that user has configured mySQL credentials
        boolean dbcredflag = true;
        if (DB_Name.compareTo("[YOUR DATABASE NAME]") == 0) {
            dbcredflag = false;
            System.out.println("You must configure a database name in the config file to use WhereYaBeen.");
        }
        if (DB_User.compareTo("[YOUR DATABASE USER]") == 0) {
            dbcredflag = false;
            System.out.println("You must configure a database user in the config file to use WhereYaBeen.");
        }
        if (DB_Password.compareTo("[YOUR DATABASE PASSWORD]") == 0) {
            dbcredflag = false;
            System.out.println("You must configure a database password in the config file to use WhereYaBeen.");
        }

        //If user has configured mySQL credentials, attempt to open the connection
        if (dbcredflag) {
            connectSQL();
        }

        //Create wyb_ tables if they do not already exist
        String createLogTable = "CREATE TABLE IF NOT EXISTS wyb_eventlog (id INT NOT NULL AUTO_INCREMENT " +
                "PRIMARY KEY, timestamp DATETIME NOT NULL, uuid VARCHAR(255) NOT NULL, eventtype INT(11) " +
                "NOT NULL, server VARCHAR(255) NOT NULL, world VARCHAR(255) NOT NULL);";
        BukkitRunnable r1 = new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Statement statement = connection.createStatement();
                    statement.executeUpdate(createLogTable);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        };
        r1.runTaskAsynchronously(Main.wyb);

        String createEventTypeTable = "CREATE TABLE IF NOT EXISTS wyb_eventtypes (id INT NOT NULL PRIMARY KEY, message VARCHAR(255) NOT NULL, description VARCHAR(255));";
        String insertEventTypes = "INSERT IGNORE INTO `wyb_eventtypes`(`id`, `message`, `description`) " +
                "VALUES ('0','performed an action','Player performed an unknown action.'),('1','joined','Player connected to a server.'),('2','quit','Player disconnected from a server.');";
        BukkitRunnable r2 = new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Statement statement = connection.createStatement();
                    statement.executeUpdate(createEventTypeTable);
                    statement.executeUpdate(insertEventTypes);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        };
        r2.runTaskAsynchronously(Main.wyb);

        getServer().getPluginManager().registerEvents(new PlayerEventListener(), this);

        //Register Commands
        this.getCommand("seen").setExecutor(new CommandSeen());

        //Future Commands
        //this.getCommand("wyb").setExecutor(new CommandWYB());
    }

    @Override
    public void onDisable() {
        //Fired when the server stops and disables all plugins
        try { //using a try catch to catch connection errors (like wrong sql password...)
            if (connection != null && !connection.isClosed()) { //checking if connection isn't null to
                //avoid recieving a nullpointer
                connection.close(); //closing the connection field variable.
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        wyb = null;
    }

    public void connectSQL() {
        if (connectionAttempts<5) {
            try { //We use a try catch to avoid errors, hopefully we don't get any.
                Class.forName("com.mysql.jdbc.Driver"); //this accesses Driver in jdbc.
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                System.err.println("jdbc driver unavailable!");
                return;
            }
            try { //Another try catch to get any SQL errors (for example connections errors)
                connection = DriverManager.getConnection(DB_URL, DB_User, DB_Password);
                //with the method getConnection() from DriverManager, we're trying to set
                //the connection's url, username, password to the variables we made earlier and
                //trying to get a connection at the same time. JDBC allows us to do this.
            } catch (SQLException e) { //catching errors)
                e.printStackTrace(); //prints out SQLException errors to the console (if any)
                connectionAttempts++;
            }
        } else {System.out.println("There have been too many connection attempts. Please restart the server to continue");}
    }
}

