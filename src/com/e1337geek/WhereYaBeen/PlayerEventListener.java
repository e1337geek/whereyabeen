package com.e1337geek.WhereYaBeen;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

public class PlayerEventListener implements Listener {

    private String query;

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player p = event.getPlayer();
        String playerName = p.getName();
        UUID playerUUID = p.getUniqueId();
        String playerServer = Main.getCurrentServer();
        String playerWorld = p.getWorld().getName();
        java.util.Date dt = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = sdf.format(dt);

        //Build SQL Query
        query = "INSERT INTO `wyb_eventlog`(`timestamp`, `uuid`, `eventtype`, `server`, `world`) VALUES ('";
        query += currentTime + "','";
        query += playerUUID + "','";
        query += "1','";
        query += playerServer + "','";
        query += playerWorld + "');";

        //Asynchronous Integration
        try {
            if (Main.connection.isValid(10)) {
                BukkitRunnable r = new BukkitRunnable() {
                    @Override
                    public void run() {
                        try {
                            Statement statement = Main.connection.createStatement();
                            statement.executeUpdate(query);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                };
                r.runTaskAsynchronously(Main.wyb);
            }
            else {
                Main.wyb.connectSQL();
                BukkitRunnable r = new BukkitRunnable() {
                    @Override
                    public void run() {
                        try {
                            Statement statement = Main.connection.createStatement();
                            statement.executeUpdate(query);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                };
                r.runTaskAsynchronously(Main.wyb);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player p = event.getPlayer();
        String playerName = p.getName();
        UUID playerUUID = p.getUniqueId();
        String playerServer = Main.getCurrentServer();
        String playerWorld = p.getWorld().getName();
        java.util.Date dt = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = sdf.format(dt);

        //Build SQL Query
        query = "INSERT INTO `wyb_eventlog`(`timestamp`, `uuid`, `eventtype`, `server`, `world`) VALUES ('";
        query += currentTime + "','";
        query += playerUUID.toString() + "','";
        query += "2','";
        query += playerServer + "','";
        query += playerWorld + "');";

        //Asynchronous Integration
        try {
            if(Main.connection.isValid(10)) {
                BukkitRunnable r = new BukkitRunnable() {
                    @Override
                    public void run() {
                        try {
                            Statement statement = Main.connection.createStatement();
                            statement.executeUpdate(query);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                };
                r.runTaskAsynchronously(Main.wyb);
            }
            else {
                Main.wyb.connectSQL();
                BukkitRunnable r = new BukkitRunnable() {
                    @Override
                    public void run() {
                        try {
                            Statement statement = Main.connection.createStatement();
                            statement.executeUpdate(query);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                };
                r.runTaskAsynchronously(Main.wyb);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
